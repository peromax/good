
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , Twit = require('twit')
  , io = require('socket.io')
  , path = require('path');



var app = express()
  , server = http.createServer(app)
  , io = io.listen(server);

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(require('connect-assets')());
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});




app.get('/', routes.index);
app.get('/users', user.list);

// twit use
var T = new Twit({
    consumer_key:         'vfEyWMpb9zgfQ2Zc9W7jSg'
  , consumer_secret:      'c9ldm2gfVSRsTJ3OtuxO3oQMGEZzouYAMfDNgNcQ0'
  , access_token:         '386134148-mI2449o6yA9k0yUJwv5JxiohRngC3SM3fTRZE6tz'
  , access_token_secret:  'xCRGQAKWc8YFPHvd3Z0Hh7KZpD0BU1wObhMFgmxpLk'
});


// twit.stream('statuses/filter', {'locations':'36.557007,55.156905,38.188477,56.056702'}, function(stream) {
//   stream.on('data', function (data) {
//     console.log(data);
//   });
// });

var stream = T.stream('statuses/filter', { track: 'fender'
  // , locations:'36.557007,55.156905,38.188477,56.056702' 
});

console.log(stream);


// twit.stream('statuses/filter', {'locations':'-122.75,36.8,-121.75,37.8,-74,40,-73,41'}, function(stream) {
//   stream.on('data', function (data) {
//     console.log(data);
//   });
// });

// stream.on('tweet', function (tweet) {
//     socket.emit('tweet', {tweet: tweet});
//   })

// socket.io listen
io.sockets.on('connection', function(socket){
  stream.on('tweet', function (tweet) {
    socket.emit('tweet', {tweet : tweet})
  }) 

  socket.on('toggle-stream', function(data){
    if(data.value == 'pause'){
      stream.stop();
    }else{
      stream.start();
    }
  });
});


server.listen(app.get('port'), function(){
  console.log("Express sdfdsf server listening on port " + app.get('port'));
});
