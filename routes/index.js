
/*
 * GET home page.
 */

rest = require('restler');
_ = require('underscore');


// helpers
function formatTwitString(str){
  str=' '+str;
  str = str.replace(/((ftp|https?):\/\/([-\w\.]+)+(:\d+)?(\/([\w/_\.]*(\?\S+)?)?)?)/gm,'<a href="$1" target="_blank">$1</a>');
  str = str.replace(/([^\w])\@([\w\-]+)/gm,'$1@<a href="http://twitter.com/$2" target="_blank">$2</a>');
  str = str.replace(/([^\w])\#([\w\-]+)/gm,'$1<a href="http://twitter.com/search?q=%23$2" target="_blank">#$2</a>');
  str = str.replace(/(\b\#good\b)/gi, '<span class="hl">$1</span>');
  return str;
}

function relativeTime(pastTime){ 
  var origStamp = Date.parse(pastTime);
  var curDate = new Date();
  var currentStamp = curDate.getTime();
  
  var difference = parseInt((currentStamp - origStamp)/1000);

  if(difference < 0) return false;

  if(difference <= 5)       return "Только что";
  if(difference <= 20)      return "Несколько секунд назад";
  if(difference <= 60)      return "Минуту назад";
  if(difference < 3600)     return parseInt(difference/60)+" минут назад";
  if(difference <= 1.5*3600)    return "Час назад";
  if(difference < 23.5*3600)    return Math.round(difference/3600)+" часов назад";
  if(difference < 1.5*24*3600)  return "Вчера";
  
  var dateArr = pastTime.split(' ');
  return dateArr[4].replace(/\:\d+$/,'')+' '+dateArr[2]+' '+dateArr[1]+(dateArr[3]!=curDate.getFullYear()?' '+dateArr[3]:'');
}


exports.index = function(req, res){
  
  rest.get('http://search.twitter.com/search.json?q=%23help&rpp=25&locale=ru&geocode=55.776573,37.705078,100mi&randomizekey=' + Math.random()*135)
    .on('complete', function(data) {
      var items = data.results;
      _.each(items, function(item){
        item.text = formatTwitString(item.text);
        item.created_at = relativeTime(item.created_at);
        console.log(item.created_at);
      });
      res.render('index', { title: 'Help', tweets: items });
    });
};